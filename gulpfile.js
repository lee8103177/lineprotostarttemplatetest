const { src, dest, parallel, series, watch } = require('gulp')
const pug = require('gulp-pug')
const sass = require('gulp-sass')
const uglifycss = require('gulp-uglifycss')
const autoprefixer = require('gulp-autoprefixer')
const babel = require('gulp-babel')
const eslint = require('gulp-eslint')
const uglify = require('gulp-uglify')
const plumber = require('gulp-plumber')
const gulpif = require('gulp-if')
const imagemin = require('gulp-imagemin')
const browserSync = require('browser-sync').create()
const gutil = require('gulp-util')
const del = require('del')
const vinylPaths = require('vinyl-paths')
const data = require('gulp-data')
const replace = require('gulp-replace')

sass.compiler = require('node-sass')

const languages = ['en', 'tc']
// const languages = ['tc']
const development = gutil.env.env === 'development'
const production = gutil.env.env === 'production'
const pugSetting = {
  basedir: 'src',
  pretty: true,
  locals: { env: gutil.env.env },
}

// html
const cleanHtml = () => {
  var p = []
  languages.forEach(function (language) {
    p.push(
      new Promise(function (resolve, reject) {
        src([`build/${language}/*.html`], { read: false })
          .on('end', resolve)
          .on('error', reject)
          .pipe(vinylPaths(del))
      })
    )
  })
  return Promise.all(p)
}
const renderPug = () => {
  var p = []
  languages.forEach(function (language) {
    p.push(
      new Promise(function (resolve, reject) {
        src('src/pages/*.pug')
          .pipe(plumber())
          .on('end', resolve)
          .on('error', reject)
          .pipe(
            data(function (file) {
              delete require
                .cache[require.resolve(`./src/languages/${language}.json`)]
              return require(`./src/languages/${language}.json`)
            })
          )
          .pipe(pug(pugSetting))
          .pipe(dest(`build/${language}`))
          .pipe(browserSync.stream())
      })
    )
  })
  return Promise.all(p)
}
const watchHtml = () =>
  watch(
    ['src/pages/*.pug', 'src/components/**/*.pug', 'src/templates/*.pug'],
    html
  )

const html = series(cleanHtml, renderPug)

// language
const watchLang = () => watch('src/languages/*.json', html)

// css
const cleanCss = () =>
  src(['build/assets/styles/main.css*'], {
    read: false,
    allowEmpty: true,
  }).pipe(vinylPaths(del))

const renderSass = () =>
  src('src/assets/styles/main.scss')
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(
      autoprefixer({
        browsers: ['> 1%', 'last 4 versions', 'not ie <= 8'],
        cascade: false,
      })
    )
    .pipe(gulpif(production, uglifycss()))
    .pipe(dest('build/assets/styles'))
    .pipe(browserSync.stream())

const watchCss = () => watch(['src/assets/styles/**/*.scss'], css)

const css = series(cleanCss, renderSass)

// js
const cleanJs = () =>
  src('build/assets/scripts/*', { read: false, allowEmpty: true }).pipe(
    vinylPaths(del)
  )
console.log(gutil.env.env)
const renderJs = () =>
  src('src/assets/scripts/*.js')
    .pipe(
      gulpif(
        gutil.env.env === 'development',
        replace('PROCESS_ENV_DEV', true),
        replace('PROCESS_ENV_DEV', false)
      )
    )
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(
      babel({
        presets: ['@babel/env'],
      })
    )
    // .on('error', function(err) {
    //     gutil.log(gutil.colors.red('[Babel Error]'))
    //     gutil.log(gutil.colors.red(err.message + '\n'))
    //     console.log(err.codeFrame)
    //     this.emit('end')
    // })
    .pipe(gulpif(production, uglify()))
    .pipe(dest('build/assets/scripts'))
    .pipe(browserSync.stream())

const moveLibJs = () =>
  src('src/assets/scripts/lib/**/*').pipe(dest('build/assets/scripts/lib'))

const watchJs = () => watch('src/assets/scripts/**/*', js)

const js = series(cleanJs, renderJs, moveLibJs)

// img
const cleanImage = () =>
  src('build/assets/images/*', { read: false, allowEmpty: true }).pipe(
    vinylPaths(del)
  )

const moveImage = () =>
  src('src/assets/images/**/*')
    .pipe(gulpif(production, imagemin()))
    .pipe(dest('build/assets/images'))

const watchImage = () => watch('src/assets/images/**/*', image)

const image = series(cleanImage, moveImage)

// doc
const cleanDoc = () =>
  src('build/assets/doc/*', { read: false, allowEmpty: true }).pipe(
    vinylPaths(del)
  )

const moveDoc = () => src('src/assets/doc/**/*').pipe(dest('build/assets/doc'))

const watchDoc = () => watch('src/assets/doc/**/*', doc)

const doc = series(cleanDoc, moveDoc)

// server
const devServer = () => {
  browserSync.init({
    server: {
      baseDir: 'build/',
      directory: true,
    },
    port: 8080,
    host: '0.0.0.0',
    ui: false,
  })
}

const renderAll = parallel(html, css, js, image, doc)
const watchAll = parallel(
  watchHtml,
  watchCss,
  watchJs,
  watchImage,
  watchDoc,
  watchLang
)
const start = series(renderAll, parallel(watchAll, devServer))

exports.build = renderAll
exports.default = start

exports.image = image
exports.watchImage = watchImage
